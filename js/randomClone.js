const logo = document.querySelectorAll(".header-logo-wrapper");
const chosenElements = document.querySelectorAll("nav, p, form, header, footer");

let clonedItems = [];

function randomClone() {
    let el = chosenElements[Math.floor(Math.random() * chosenElements.length)];
    let div = document.createElement("div");
    div.setAttribute("id", "logoClone");
    div.innerHTML = logo[0].innerHTML;

    if (clonedItems.length > 0) {
        document.getElementById("logoClone").remove();
    }

    el.appendChild(div);
    clonedItems.push(div);
}

