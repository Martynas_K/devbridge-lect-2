function validation(event) {
    let firstName, lastName, message;
    let firstNameAlert = '';
    let lastNameAlert = '';
    let messageAlert = '';
    let required = "Field required!";

    firstName = document.getElementById("first-name").value.trim();
    lastName = document.getElementById("last-name").value.trim();
    message = document.getElementById("message").value.trim();

    document.getElementById("message-alert").classList.remove('success');

    if (!firstName) {
        firstNameAlert = required;
    } else if (firstName.length > 50) {
        firstNameAlert = "Must be no more than 50 symbols.";
    }

    if (!lastName) {
        lastNameAlert = required;
    } else if (lastName.length > 100) {
        lastNameAlert = "Must be no more than 100 symbols.";
    } else if (lastName.match(/^[0-9]+$/) === null) {
        lastNameAlert = "Must be numbers only.";
    }

    if (!message) {
        messageAlert = required;
    } else if (message.length > 150) {
        messageAlert = "Must be no more than 150 symbols";
    }

    if (!firstNameAlert && !lastNameAlert && !messageAlert) {
        messageAlert = 'Success!';
        document.getElementById("message-alert").classList.add('success');
    }

    document.getElementById("first-name-alert").innerHTML = firstNameAlert;
    document.getElementById("last-name-alert").innerHTML = lastNameAlert;
    document.getElementById("message-alert").innerHTML = messageAlert;

    event.preventDefault();
}

const form = document.getElementById('form');
form.addEventListener('submit', validation);