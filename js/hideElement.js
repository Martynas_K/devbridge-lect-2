const elements = document.querySelectorAll(
    ".primary-navbar-item, .secondary-navbar-item, .dropdown-content-item, .footer-nav-item"
);
const hidden = document.querySelectorAll(".top-section-aside")

for (const element of elements) {
    element.addEventListener('click', function(event) {
        hidden[0].classList.add('hide');
    })
}
